import { Component, OnInit } from '@angular/core';
import { CreditPageComponent } from '../credit-page/credit-page.component';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit {
  usersText = 'test';

  constructor() { }

  ngOnInit() {
  }

  changeInput() {
    this.usersText = 'CHANGED';
  }
}

